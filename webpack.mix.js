const mix = require('laravel-mix');


mix.sass('src/sass/app.scss', 'assets/css/');

mix.disableSuccessNotifications();
